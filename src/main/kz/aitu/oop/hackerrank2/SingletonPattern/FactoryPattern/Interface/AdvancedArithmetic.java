package main.kz.aitu.oop.hackerrank2.SingletonPattern.FactoryPattern.Interface;

public interface AdvancedArithmetic {
    int divisor_sum(int n);
}
