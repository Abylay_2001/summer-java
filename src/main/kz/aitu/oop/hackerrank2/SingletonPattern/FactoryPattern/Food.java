package main.kz.aitu.oop.hackerrank2.SingletonPattern.FactoryPattern;

public interface Food {
    public String getType();
}
