package main.kz.aitu.oop.examples.task2;

public class MyString {
    private int[] values;

    public MyString(int[] values){
        this.values=values;
    }

    public int length(){
        return values.length;
    }
    public int valueAt(int position){
        if(position<0 || position>=length()) return -1;
        return values[position];
    }

    public boolean contains(int value){

        for(int i = 0; i < length(); i++) {
            if(values[i] == value) return true;
        }
        return false;

    }

    public int count(int value){
        int count = 0;
        
        return count;
    }
}
