package main.kz.aitu.oop.hackerrank.Inheritance1;

public class Solution{

    public static void main(String args[]){

        Bird bird = new Bird();
        bird.walk();
        bird.fly();
        bird.sing();

    }
}
