package main.kz.aitu.oop.assignment.Singleton;

public class Main {

    public static void main(String[] args) {

        //  SingletonPattern class with variable 1
        SingletonPattern x = SingletonPattern.getInstance();

        //  SingletonPattern class with variable 2
        SingletonPattern y = SingletonPattern.getInstance();

        //  SingletonPattern class with variable 3
        SingletonPattern z = SingletonPattern.getInstance();

        System.out.println( x.a);
        System.out.println( y.a);
        System.out.println( y.a);
    }
}

