package main.kz.aitu.oop.assignment.Singleton;

class SingletonPattern
{
    // static variable single_instance of type Singleton
    private static SingletonPattern single_instance = null;

    // variable of type String
    public String a;

    // private constructor
    private SingletonPattern()
    {
        a = "Intro to Singleton pattern";
    }

    // static method to create instance of Singleton class
    public static SingletonPattern getInstance()
    {
        if (single_instance == null)
            single_instance = new SingletonPattern();

        return single_instance;
    }
}
