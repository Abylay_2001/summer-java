package main.kz.aitu.oop.assignment.Singleton.Factory;
//Here we create a instance of each color of Car(red,green)
public class Main {
    public static void main(String[] args) {
        //create a car with color red
        Car car=CarFactory.getCar("red");
        car.speed();
        //create a car with color green
        car=CarFactory.getCar("green");
        car.speed();
    }
}
