package main.kz.aitu.oop.assignment.Singleton.Factory;
public class CarFactory {
    //CarFactory has a static method getCar, that returns a Car, which depends on the color
    public static  Car getCar(String color){
        if(color.equals("red")){
            return new Audi();
        }else if(color.equals("green")){
            return new Toyota();
        }
        return null;
    }
}
