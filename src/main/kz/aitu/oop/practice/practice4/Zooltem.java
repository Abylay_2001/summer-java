package main.kz.aitu.oop.practice.practice4;

abstract class ZooItem {
    private static int id = 0;
    private String name;
    private int price;

    public ZooItem(String name, int price) {
        this.id++;
        this.name = name;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
