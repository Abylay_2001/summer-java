package main.kz.aitu.oop.practice.practice4;

public class Main {
    public static void main(String[] args) {
        Aquarium aquarium = new Aquarium();
        aquarium.addFish(new Angelfish("Sally", 12000));
        aquarium.addFish(new Goldfish("Bobby", 16000));
        aquarium.addFish(new Rainbowfish("Kelly", 24000));
        aquarium.addReptile(new Snake("Jack", 13000));
        aquarium.addReptile(new Lizard("Locky", 13000));
        aquarium.addAccessory(new Stone("Golden stone", 20000));

        System.out.println("Aquarium total cost: " + aquarium.getTotalCost());
    }
}
