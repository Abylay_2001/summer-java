package main.kz.aitu.oop.practice.practice4;

import java.util.ArrayList;

public class Aquarium implements IAquariumLogic{
    private ArrayList<Fish> fishes; // each aquarium should have some fishes
    private ArrayList<Reptile> reptiles;  // each aquarium should have some reptiles
    private ArrayList<Accessory> accessories;  // each aquarium should have some accessories

    public Aquarium() {
        this.fishes = new ArrayList<>();
        this.reptiles = new ArrayList<>();
        this.accessories = new ArrayList<>();
    }

    public void addFish(Fish fish){
        this.fishes.add(fish);
    }

    public void addReptile(Reptile reptile){
        this.reptiles.add(reptile);
    }

    public void addAccessory(Accessory accessory){
        this.accessories.add(accessory);
    }

    @Override
    public int getTotalCost() {
        int totalCost = 0;

        for(Fish f : fishes){ // sum the all fish prices
            totalCost += f.getPrice();
        }

        for(Reptile r : reptiles){ // sum the all reptile prices
            totalCost += r.getPrice();
        }

        for(Accessory a : accessories){ // sum the all accessory prices
            totalCost += a.getPrice();
        }

        return totalCost; //returning the total price
    }
}

